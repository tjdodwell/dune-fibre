#include <iostream>
#include <math.h>
#include <armadillo>

using namespace std;
using namespace arma;

#include "classes/fibre.hh"
#include "functions/VMF_distributions.hh"
#include "functions/DEM.hh"
#include "functions/writeVTK.hh"


int main(int argc, char** argv)
{

arma_rng::set_seed_random();

int dim = 3; // Dimension of Simulation - (Nearly ways 3)
double L = 1.0; // User Input Length of Cube - i.e. V := (0,L)^dim
bool periodic = true;
int numFibres = 10; // User Input Number of Fibre
double r = 0.01; // User Input Radius of Fibre
double ds = 2 * r; // User Input Initial distance between adjacent particles

double E = 1.0;

double beta1 = 0.05;
double beta2 = 0.1;

double k0 = 0.0001; // User Input for VMF distribution - weighting factor for laying direction
double k1 = 100.0; // User Input for VMF distribution - weighting factor for last particle direction
vec mu0(3); mu0[0] = 1.0; mu0[1] = 0.0; mu0[2] = 0.0; // User Input for VMF Distribution - user defined prefered layer direction

double res_tol = 0.1; // Force Residual for Particle Simulation Step
double dt = 1.0;

// End of User Define Values

// << 1 >> Setup of (Overlapping) Fibre Network

int MAX_PARTICLES = numFibres * 100;

int numParticles = 0;

std::vector<fibre> f(numFibres);
std::vector<int> p2f(MAX_PARTICLES);
std::vector<int> p2l(MAX_PARTICLES);

for (int i = 0; i < numFibres; i++){

// Process for setuping up a fibre and particles

f[i].setId(i); // Define Fibre ID

f[i].setRadius(r); // Define radius of fibre (constant along length)

f[i].setDS(ds); // Defines the step size between particles along a chain

f[i].setLength(100.0 * r * randu()); // Define length of fibre (as double)

/*
Behind the scene function setLength calculates int numParticles = l / ds + 1 the number of particles along a fibre length

It then creates a vector of "particles" (from particles.hh - nested class) of length numParticles and also a vector of interger L2G which will store local (fibre) to Global number of the particles
*/

for (int j = 0; j < f[i].getNumParticles(); j++){
	numParticles += 1;// Increment Global Particle Counter
	f[i].setParticleID(j,numParticles - 1);
	p2f[numParticles - 1] = i;
	p2l[numParticles - 1] = j;
}


f[i].setX(0,L * randu<vec>(dim)); // Initialise Random Start Position in V := (0,L)^dim

f[i].setV(0,VMF_sample(k0,k0,mu0,mu0)); // Initial Random Fibre Direction from Von-Mises Fisher Distribution ( see function/VMF_distribution.hh )

f[i].setF(0,zeros<vec>(3)); // Initial force on particle to zero for particle simulation (step 2)
f[i].setdXdt(0,zeros<vec>(3)); // Initial Velocity for Particle Simulation

	for (int j = 1; j < f[i].getNumParticles(); j++){

		vec mu1 = f[i].getV(j-1); // Get the direction of the fibre at the last particle

		f[i].setX(j,f[i].getX(j-1) + ds * mu1,periodic,L); // Find position of next particle from position and direction of fibre at last particle

		f[i].setV(j,VMF_sample(k0,k1,mu0,mu1)); // Generate direction of fibre at new particle, with a preferred direction set to direction of fibre at last particle

		f[i].setdXdt(j,zeros<vec>(3)); //Initial velocity of particle
		f[i].setF(j,zeros<vec>(3)); // Initial force on particle to zero for particle simulation (step 2)

	}

	

}

    cout << numParticles << endl;

mat DS = zeros<mat>(numParticles,numParticles);

for (int i = 0; i < numParticles; i++){
	int f_i = p2f[i];	int l_i = p2l[i];
	for (int j = 0; j < numParticles; j++){
		int f_j = p2f[j];	int l_j = p2l[j];

		vec x1 = f[f_i].getX(l_i);
		vec x2 = f[f_j].getX(l_j);
    

		DS(i,j) = dist(x1,x2);
        
        //cout << DS(i,j) << endl;
        
        if (DS(i,j) > sqrt(3.0)){ std::cout << "ERROR"; return 1;}


	}
}

    
// Write (Overlapping) output to vtk file for visualisation
writeVTK<std::vector<fibre> >("Overlapping.vtk",f,p2f,p2l,numParticles);
    

    
int numGridPoints = 100;
    
cube material = zeros<cube>(numGridPoints,numGridPoints,numGridPoints);
    
vec coords = linspace<vec>(0,L,numGridPoints);

    for (int i = 0; i < numGridPoints; i++){
        for (int j = 0; j < numGridPoints; j++){
            for (int k = 0; k < numGridPoints; k++){
                
                vec point = zeros<vec>(3);
                point[0] = coords[i]; point[1] = coords[j]; point[2] = coords[k];
                
                for (int ip = 0; ip < numParticles; ip++){
                    
                    int f_i = p2f[ip];	int l_i = p2l[ip];
                    
                    vec x1 = f[f_i].getX(l_i);
                    
                    if (dist(x1,point) < r){
                        material(i,j,k) = 1.0; //Fibre
                    }
                    
                }
                
                
            }
        }
    }
    
    
    
/*
<< 2 >> Generate Non-Overlapping Fibre Network using Particle Simulation
*/
double residual = 2.0 * res_tol;

p2f.resize(numParticles); p2l.resize(numParticles);

while (residual > res_tol){

	residual = 0.0;


	// Update Velocities and Displacements of Each Particle

	for (int i = 0; i < numParticles; i++)
	{
		int f_id = p2f[i];
		int l_id = p2l[i];

		f[f_id].setdXdt(l_id, f[f_id].getdXdt(l_id) + 0.5 * dt * f[f_id].getF(l_id));		

		f[f_id].setX(l_id,f[f_id].getX(l_id) + dt * f[f_id].getdXdt(l_id));				
	}

	// Compute Force on Each particle

	for (int i = 0; i < numParticles; i++)
	{
		vec F = zeros<vec>(3); // Initialise Force on a particle

		int f_id = p2f[i];
		int l_id = p2l[i];

		vec neighID = f[f_id].getNeigh(l_id);

		if (neighID.size() == 2){
			//cout << " DO something here!" << endl;
		}

		vec x1 = f[f_id].getX(l_id);

		for (int k = 0; k < neighID.size(); k++){ // for each neighbour
			// fibre id (should be) the same
			int n_id = p2l[neighID[k]];
			vec x2 = f[f_id].getX(n_id);
			F += Fa(x1,x2,DS(l_id,n_id),E,r,beta1,beta2);
		}

		

		for (int j = 0; j < numParticles; j++){

			bool calculate_Contact = true;

			if (i == j){calculate_Contact = false;}


			int test_sum = 0;
			for (int k = 0; k < neighID.size(); k++){
				if(j == neighID[k]){ test_sum += 1; }
				if (test_sum > 0){ calculate_Contact = false; }
			}

			if (calculate_Contact){

				int f_id = p2f[j];
				int n_id = p2l[j];

				vec x2 = f[f_id].getX(n_id);

				F += Frp(x1,x2,r,r);

			}

			f[p2f[i]].setF(p2l[i],F);
            
            

			residual += L2(F);

		}




	}


}

return 0;

}