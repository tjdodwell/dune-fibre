class cell{

public:

	cell(){}; // Default Constructor

	void inline setX(vec tmp){	x = tmp;	}

	void inline setNeighbour(int j) { 
		int current_size = neighbours.size();
		neighbours.resize(current_size + 1);
		neighbours[current_size] = j;
	}

private:

	std::vector<double> x(3);
	int numNeigh;
	std::vector<int> neighbours;

}




/*Cell lists

Divide domain into subdomains - with an edge length equal or greater than to the cut-off radius of interaction to be computed. The particles are sorted into these cells and the interactions are computed between in the same and neighbouring cells
*/


int cell_1D = 2;

vec x = linspace<vec>(0, L, cell_ID + 1);

int cells = std::pow(cell_1D,3);

// ------------------------------------------------
// 			Compute centre of each cell
// ------------------------------------------------
std::vector<cell> cells(numCells);
vec tmp = zeros<vec>(3);
int counter = 0;
for (int i = 0; i < cell_1D; i++){
	tmp[0] = 0.5 * (x[i + 1] + x[i]);
	for (int j = 0; j < cell_1D; j++){
		tmp[1] = 0.5 * (x[j + 1] + x[j]);
		for (int k = 0; k < cell_1D; k++){
			tmp[2] = 0.5 * (x[k + 1] + x[k]);
			cells[counter].setX(tmp);
			counter += 1;
		}
	}
}
// ------------------------------------------------


// ------------------------------------------------
// 			Compute cell neighbours
// ------------------------------------------------

double rc = L / 10.0;

for (int i = 0; i < numCells; i++){
	for ( int j = 0; j < numCells; j++){
		if (i != j){
			double d = dist(cells[i].x,cells[j].x);
			if (d < rc){cells[i].setNeighbour(j);}
		}
	}
}
// ------------------------------------------------


// ------------------------------------------------
// 				Generate Cell List
// ------------------------------------------------

std::vector<int> cell_head(numCells) = -1;
std::vector<int> cell_last(numCells) = -1;

std::vector<int> cell_list(numParticles) = -1;

std::vector<int> particlesInCell(numCells);

for (int i = 0; i < numParticle; i++){

	int f_i = p2f[i];	int l_i = p2l[i];
	vec x1 = f[f_i].getX(l_i);

	double dmin = L;
	int cell_indx = numCells + 1;

		// Loop through each cell and find the closest
		for (int j = 0; j < numCells; j++){
			double d = dist(cells[j].x,x1);
			if (d < dmin){dmin = d; cell_indx = j;}
		}

		if (particleInCell[cell_indx] = 0){
			// First Particle in the Cell
			cell_head[cell_indx] = i;
			cell_last[cell_indx] = i;
			particlesInCell[cell_indx] += 1; 
		}
		else{
			cell_list[i] = cell_last[cell_indx];
			cell_last[cell_indx] = i;
		}

}
// ------------------------------------------------







