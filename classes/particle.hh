template<typename T, int dim>
class particle{


public:

	particle(): x(dim) {}; // Default Constructor

	void inline setNeighbours(int n, std::vector<int>& ids){
		numNeigh = n;
		NeighIDs = ids;
	}

	void inline setGlobalID(int id){ globalID = id; }

	void inline setPoint(vec coords){ x = coords; }

	int inline getGlobalID(){ return = globalID;}
	int inline getnumNeigh(){ return numNeigh; }
	int inline getNeighIDs(){ return NeighIDs; }



private:

	T x;
	std::vector<int> NeighIDs;
	int numNeigh;
	int globalID;


}