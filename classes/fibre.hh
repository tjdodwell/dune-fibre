
class fibre{

public:

	fibre () {}; // Default Constructor

	void inline setId(int fibre_id){ id = fibre_id; }
	int inline getId(){return id;}

	void inline setRadius(double radius){r = radius;}
	void inline setDS(double dl){ds = dl;}
	void inline setLength(double l){
		length = l;
		l_int = l / ds;
		numParticles = l_int + 1;
		X.resize(numParticles);
		dXdt.resize(numParticles);
		V.resize(numParticles);
		F.resize(numParticles);
		l2G.resize(numParticles);
		numNeigh.resize(numParticles);
	}

	void inline setParticleID(int localID, int globalID){
		l2G[localID] = globalID;
	}


	void inline setX(int localID, vec x, bool periodic = false, double L = 1.0){
		if (periodic == true){
			for (int k = 0; k < x.size();k++)
            { // For each dimension
				if (x[k] < 0){
					x[k] = L + x[k];
				}
				else if (x[k] > L){
					x[k] = x[k] - L;
				}
			}
		}
		X[localID] = x;
	}

	void inline setdXdt(int localID, vec dxdt){
		dXdt[localID] = dxdt;
	}

	void inline setV(int localID, vec v){
		V[localID] = v;
	}

	void inline setF(int localID, vec force){
		F[localID] = force;
	}

	double inline getLength(){return length;}
	double inline getRadius(){return r;}

	int inline getNumParticles(){return numParticles;}

	vec inline getX(int particle_id){

		vec output = X[particle_id];

		return output;
	}

	vec inline getdXdt(int particle_id){

		vec output = dXdt[particle_id];

		return output;
	}

	vec inline getV(int particle_id){
		vec output = V[particle_id];
		return output;
	}

	vec inline getF(int particle_id){
		vec output = F[particle_id];
		return output;
	}

	vec inline getNeigh(int p_id){
		vec output(2);
		if (p_id == 0){
			output.resize(1);
			output[0] = l2G[1];

		}
		else if (p_id == numParticles - 1){
			output.resize(1);
			output = l2G[p_id - 1];
		}
		else{
			output[0] = l2G[p_id - 1]; output[1] = l2G[p_id + 1];
		}
		return output;
	}

private:

	int id;
	int numParticles;
	std::vector<vec> X;
	std::vector<vec> dXdt;
	std::vector<vec> V;
	std::vector<vec> F;
	std::vector<int> l2G;
	std::vector<int> numNeigh;
	double r; // Fibre Radius
	double ds;
	double length; // Fibre length as units of fibre radius
	int l_int;

};




