#include <iostream>
#include <math.h>
#include <armadillo>

using namespace std;
using namespace arma;

#include "classes/fibre.hh"
#include "functions/VMF_distributions.hh"


int main(int argc, char** argv)
{


arma_rng::set_seed_random();

double L = 1.0;

bool periodic = true;

int numFibres = 100;

double radius = 0.05;

std::vector<fibre> f(numFibres);
for (int i = 0; i < numFibres; i++){
	f[i].setId(i);
	f[i].setRadius(radius);
	double tmp = 20.0 * radius * randu();
	f[i].setLength(tmp);
}


double k1 = 200.0, k2 = 1.0;

vec mu0(3);
mu0[0] = 1.0;	mu0[1] = 0.0; mu0[2] = 0.0;



for (int j = 0; j < numFibres; j++){

	vec x = zeros<vec>(3);

	vec mu1(3), mu2(3);
	mu1 = pdf_mvarMisesFisher(k1,k1,mu0,mu0);
	mu2 = mu1;

	double r = f[j].getRadius();

	int length_int = f[j].getLength() / radius;


	for (int i = 0; i < length_int; i ++){

		vec v(3);
		v = pdf_mvarMisesFisher(k1,k2,mu1,mu2);

		mu2 = v;

		x = x + 0.5 * r * v;

		// if periodic, need to add it here!

		if (periodic == true){
			for (int k = 0; k < 3;k++){
				if (x[k] < 0){
					x[k] = L + x[k];
				}
				else if (x[k] < L){
					x[k] = x[k] - L;
				}
			}
		}

		f[j].setPoint(i,x);
		f[j].setDirector(i,v);

	}

}

return 0;


}