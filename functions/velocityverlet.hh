
template<typename P>

double velocityVerlet_step()
{

int dim = 3;

double residual = 0.0;


for (int i = 0; i < numParticles; i++){
	vhalf[i] = vold[i] + 0.5 * dt * fold[i];
	x[i] = xold[i]  + dt * vhalf[i];
}

// compute fnew = f(xnew)

for (int i = 0; i < numParticles; i++){

	F[i] = 0.0; // Initialise force vector on particle = 0.0

	// For immediate particles only - Compute
	int numNeighbours = p[i].getnumNeigh();

	std::vector<int> neighbour_ids(numNeighbours);
	neighbour_ids = p[i].getNeighbourIDs();

	if (numNeighbours == 2){ 
		F[i] += Fr<vec>(x[i],x[neighbour_ids[0]],x[neighbour_ids[1]])
	}

	for (int j = 0; j < numNeighbours; j++){
		F[i] += Fa<vec>(x[i],x[neighbour_ids[j]],ds,E);
	}


	// << B >> Loop through each particle and calculate force due to overlap
	for (int j = 0; j < numParticles; j++){

		if (j != i && p[i].notNeighbour(j)){ // If not the same particle
			F[i] += Frp<vec>(x[i],x[j]);
		}

	} // End loop through each particle

	for (int j = 0; j < dim; j++){ residual += F[i][j];}

	vnew[i] = vhalf[i] + 0.5 * dt * F[i];


} // For each particle


}