double dot(vec& x, vec& y){
	double d = 0.0;
	for (int i = 0; i < x.size(); i++){
		d += x[i] * y[i];
	}
	return d;
}

double len_vec(vec& x){double l = std::sqrt(dot(x,x)); return l;}

double L2(vec& x){double l = std::sqrt(dot(x,x)); return l;}

double dist(vec& x1, vec& x2){
	vec dx = x1 - x2;
	double d = len_vec(dx);
	return d;
}

vec direction(vec& x1, vec& x2){
	double d = dist(x1,x2);
	vec v(x1.size());
	for (int i = 0; i < x1.size(); i++){
		v[i] = (x1[i] - x2[i]) / d;
	}
	return v;
}

/*
template <typename T,int dim>
T inter_vector(T& x1, T& x2, bool normalised = false){
	T output = x2 - x1;
	if (normalised){
		output /= dist<T,dim>(x1,x2);
	}
	return output;
}

template<typename T>
double angle_between_vectors(T& x, T& y){
	double alpha = std::acos(dot<T>(x,y) / (len<T>(x) * len<T>(y));
	return alpha;
}*/

double Overlap(double r1, double r2, vec& x1, vec& x2){
// Overlap - computes to overlap between two particles
	double d = dist(x1,x2);
	double output = std::max(0.0,r1 + r2 - d);
	return output;
}

vec Frp(vec& x1, vec& x2,double r1, double r2){
	int dim = x1.size();
	double d = dist(x1,x2);
	vec Frp(dim); // Initialise repulsive force
	if (d == 0){Frp = 0.0;} // Return 0 as same point.}
	else{Frp = Overlap(r1,r2,x1,x2) * (x1 - x2);}
	return Frp;
}

// Forces and Moments for P
double fric(double beta1, double beta2, double x){
	double f;
	if (x < beta1){f = 0.0;}
	else if (x > beta2) {f = 1.0;}
	else{	f = 0.5 - 0.5 * std::cos(M_PI * (std::abs(x) - beta1)/(beta1 - beta2));}
	return f;
}


vec Fa(vec& x1,vec& x2, double ds, double E, double r,double beta1, double beta2){

	double axial_strain = (ds - dist(x1,x2)) / ds;
	double k = E * 0.5 * M_PI * r * r;
	vec F = k * fric(beta1,beta2,axial_strain) * axial_strain * direction(x1,x2);
	return F;
}







