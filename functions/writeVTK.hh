#include <fstream>
#include <sstream>

template <typename T>
T str2type( const string &Text ){                               
	stringstream ss(Text);
	T result;
	return ss >> result ? result : 0;
}

template <typename Fs>
void writeVTK(const string filename, Fs& f,std::vector<int>& p2f, std::vector<int>& p2l, int numParticles){

ofstream out(filename);

  		out << "# vtk DataFile Version 3.0\n";
  		out << "TITLE\t \n";
  		out << "ASCII\n";
  		out << "DATASET POLYDATA\n";
  		out << "\n";
  		out << "POINTS " << numParticles << " float\n";


  		for (int i = 0; i < numParticles; i++){
  			int fibre_id = p2f[i];
  			int local_id = p2l[i];
  			vec x = f[fibre_id].getX(local_id);
  			out << x[0] << " " << x[1] << " " << x[2] << "\n"; 
  		}

  		out << "\n";

  		out << "VERTICES " << numParticles << " " << 2 * numParticles << "\n";

  		for (int i = 0; i < numParticles; i++){
  			out << "1" << " " << i << "\n";
  		}

  		out << "\n";
  		out << "POINT_DATA " << numParticles << "\n";
  		out << "SCALARS Fibre_id float\n";
  		out << "LOOKUP_TABLE default\n";

  		for (int i = 0; i < numParticles; i++){
  			out << p2f[i] << "\n";
  		}

 		out.close();



}

