

double VMF_pdf(double beta, double theta, double phi){
// pdf_MisesFisher - returns the pdf of value for the von Mises-Fisher Distribution
	double f = beta * std::sin(theta);
	double tmp = std::pow(4.0 * M_PI * (1.0 + (beta * beta - 1) * std::cos(theta) * std::cos(theta)),1.5);
	f /= tmp;
	return f;
}

mat VMF_sample(double k1, double k2, mat& mu1, mat& mu2){
// pdf_mvarMisesFisher - returns the pdf of value for the multivariant von Mises-Fisher Distribution
	arma_rng::set_seed_random();

	// Normalise Directions
	normalise(mu1);
	normalise(mu2);


	// Calculate k = |k1 * mu1 + k2 * mu2|
	double k = 0.0;
	for (int i = 0; i < 3; i++){
		k += std::pow(k1 * mu1(i,0) + k2 * mu2(i,0),2);
	}
	k = std::sqrt(k);

	// Sample from inverse CDF
	double xi = randu();
	double invF = 1 + (1 / k) * std::log(xi + (1 - xi) * std::exp(-2.0 * k));
	double angle = 2 * M_PI * randu(); // \theta ~ U(0,2 * M_PI]

	mat v(3,1);
	v(0,0) = invF;	
	v(1,0) = std::sqrt(1 - invF * invF) * std::cos(angle);	
	v(2,0) = std::sqrt(1 - invF * invF) * std::sin(angle);

	// Realign mean rotation
	mat mu(3,1);
	mu = (k1 * mu1 + k2 * mu2) / (k1 + k2);

	double ty = std::asin(mu[2]);

	double tz;

	if (std::abs(std::abs(ty) - M_PI * 0.5) < 1e-6){
    	tz = 0.0;
	}
	else{   
    	tz = std::acos(mu[0] / std::cos(ty));
  	}

  	mat Ry = eye(3,3);
	Ry(0,0) = std::cos(ty); Ry(0,2) = std::sin(ty);
	Ry(2,0) = -std::sin(ty); Ry(2,2) = std::cos(ty);

	mat Rz = eye(3,3);
	Rz(0,0) = std::cos(tz); Rz(0,1) = -std::sin(tz);
	Rz(1,0) = std::sin(tz); Rz(1,1) = std::cos(tz);

	mat output = Ry * Rz * v;

	return output;

}